local prod = true

if (prod == false) then
	data:extend({
		{
			type = "bool-setting",
			name = "belt-brush_debug-mode",
			setting_type = "runtime-global",
			default_value = false
		}
	})
end

data:extend({
	{
		type = "bool-setting",
		name = "belt-brush_hide-icon",
		setting_type = "runtime-per-user",
		default_value = true
	}
})

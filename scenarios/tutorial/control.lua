local function init_stuff()
  local surface = game.surfaces["tutorial"]

  -- Adds tips
  for i = 1, 5 do
    local entity = game.get_entity_by_tag("robot-" .. i)
    entity.active = false
    rendering.draw_text{
      text = {"bb-tutorial.robot-" .. i},
      surface = surface,
      color = {1, 1, 1},
      target = entity,
      target_offset = {0, -2.1},
      alignment = "center",
      visible = true
    }
  end
  rendering.draw_circle{
    color = {},
    radius = 0.2,
    filled = true,
    target = {9.5, -10.5},
    surface = surface,
    visible = true
  }
  rendering.draw_line{
    color = {},
    width = 3,
    from = {9.5, -10.5},
    to = {9.7, -15.4},
    surface = surface,
    visible = true
  }
  rendering.draw_line{
    color = {},
    width = 2,
    from = {9.7, -15.4},
    to = {10.5, -15.5},
    surface = surface,
    visible = true
  }
  rendering.draw_circle{
    color = {},
    radius = 0.2,
    filled = true,
    target = {9.5, -68.5},
    surface = surface,
    visible = true
  }
  rendering.draw_line{
    color = {},
    width = 3,
    from = {9.5, -68.5},
    to = {9.5, -71.5},
    surface = surface,
    visible = true
  }
  rendering.draw_circle{
    color = {},
    radius = 0.15,
    filled = true,
    target = {10.5, -71.5},
    surface = surface,
    visible = true
  }
  rendering.draw_line{
    color = {},
    width = 3,
    from = {10.5, -71.5},
    to = {10.5, -72.5},
    surface = surface,
    visible = true
  }
  rendering.draw_line{
    color = {},
    width = 2,
    from = {10.5, -72.5},
    to = {11.5, -72.5},
    surface = surface,
    visible = true
  }
end

script.on_event(defines.events.on_player_created, function(event)
  if event.player_index == 1 then
    init_stuff()
  end

  local surface = game.surfaces["tutorial"]
  local player = game.players[event.player_index]
  local new_position = surface.find_non_colliding_position("character", {7, -0.4}, 30, 0.1)
  if new_position then
    player.teleport(new_position, surface)
  end
  player.print({"bb-tutorial.first-message"})
end)

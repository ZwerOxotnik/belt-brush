# Belt brush

Read this in another language | [English](/README.md) | [Русский](/docs/ru/README.md)
|---|---|---|

## Contents

* [Overview](#overview)
* [Issues](#issue)
* [Features](#feature)
* [Installing](#installing)
* [License](#license)

## Overview

Allows to draw properly-directed belts like tiles. Draw belts with a single drag! There is a tutorial in scenarios.

### Usage

* Craft "Belt brush"
* Decrease the belt brush set size to 1 (Numpad-)
* Draw 5 tiles to make it work

Belts: drag in required direction
Underground belts: drag through built entities
Splitters: move mouse 1 tile back diagonaly and drag again

To change belt tier, use a button on top.
The button is hidden while player is not holding brush by default (changeable in per-player settings)

## <a name="issue"></a> Found an Issue?

Please report any issues or a mistake in the documentation, you can help us by [submitting an issue][issues] to our GitLab Repository or on [mods.factorio.com][mod portal] or on [forums.factorio.com][homepage].

## <a name="feature"></a> Want a Feature?

You can *request* a new feature by [submitting an issue][issues] to our GitLab Repository or on [mods.factorio.com][mod portal] or on [forums.factorio.com][homepage].

## Installing

If you have downloaded a zip archive:

* simply place it in your mods directory.

For more information, see [Installing Mods on the Factorio wiki](https://wiki.factorio.com/index.php?title=Installing_Mods).

If you have downloaded the source archive (GitLab):

* copy the mod directory into your factorio mods directory
* rename the mod directory to belt-brush_*versionnumber*, where *versionnumber* is the version of the mod that you've downloaded (e.g., 0.4.1)

## License

Use of the source code included here is governed by the MIT licence. See the [LICENCE](/LICENCE) file for details.

[issues]: https://gitlab.com/ZwerOxotnik/belt-brush/issues
[mod portal]: https://mods.factorio.com/mod/belt-brush/discussion
[homepage]: https://forums.factorio.com/viewtopic.php?f=190&t=66922
[Factorio]: https://factorio.com/

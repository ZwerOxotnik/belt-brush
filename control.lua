--[[
 
Copyright (c) 2018-2020 Dimava
Copyright (c) 2018-2020 ZwerOxotnik <zweroxotnik@gmail.com>
Licensed under the MIT licence;
 
You can write and receive any information on the links below.
Source: https://gitlab.com/ZwerOxotnik/belt-brush
Mod portal: https://mods.factorio.com/mod/belt-brush
Homepage: https://forums.factorio.com/viewtopic.php?f=190&t=66922
 
]]--

local debug_mode = false
if (settings.global["belt-brush_debug-mode"] ~= nil) then
	debug_mode = settings.global["belt-brush_debug-mode"].value or false
end

local function check_belts()
	local belt_groups = {}
	local underground_belts = {}
	local splitters = {}
	for _, entity in pairs(game.entity_prototypes) do
		if entity.type == "transport-belt" then
			local is_valid = false
			if entity.items_to_place_this then
				for _, item in pairs(entity.items_to_place_this) do
					if item.name == entity.name then
						is_valid = true
					end
				end
			end
			if entity.name:find("^replicating-") then
				is_valid = false
			end

			if is_valid then
				table.insert(belt_groups, {belt = {name = entity.name}})
			-- else
				--table.insert(belt_groups, {belt = {name = nil}})
			end
		elseif entity.type == "underground-belt" then
			local is_valid = false
			if entity.items_to_place_this then
				for _, item in pairs(entity.items_to_place_this) do
					if item.name == entity.name then
						is_valid = true
					end
				end
			end

			if is_valid then
				table.insert(underground_belts, {name = entity.name, underground_length = entity.max_underground_distance})
			-- else
				--table.insert(underground_belts, {name = nil})
			end
		elseif entity.type == "splitter" then
			local is_valid = false
			if entity.items_to_place_this then
				for _, item in pairs(entity.items_to_place_this) do
					if item.name == entity.name then
						is_valid = true
					end
				end
			end

			if is_valid then
				table.insert(splitters, {name = entity.name})
			-- else
				--table.insert(splitters, {name = nil})
			end
		end
	end

	log('belts: ' .. serpent.block(belt_groups))
	log('ubelts: ' .. serpent.block(underground_belts))
	log('sbelts: ' .. serpent.block(splitters))

	for k, belt in pairs(underground_belts) do
		if belt_groups[k] then
			belt_groups[k].underground_belt = belt
		end
	end
	for k, splitter in pairs(splitters) do
		if belt_groups[k] then
			belt_groups[k].splitter = splitter
		end
	end

	local is_unstable = false
	local belt_groups_final = {}
	for k, data in pairs(belt_groups) do
		if not (data.underground_belt and data.underground_belt.name
			and data.splitter and data.splitter.name
		and data.belt and data.belt.name) then
		belt_groups[k] = nil
		is_unstable = true
	else
		table.insert(belt_groups_final, data)
	end
end

if debug_mode then log(serpent.line(belt_groups)) end
if is_unstable then game.print("Perhaps the \"Belt brush\" mod is tangled up!", {1, 1, 0}) end

global.belt_brush.belt_groups = belt_groups_final
end

local function on_init()
	global.belt_brush = global.belt_brush or {}
	local belt_brush = global.belt_brush
	belt_brush.players = belt_brush.players or {}
	belt_brush.belt_groups = belt_brush.belt_groups or {}

	if game then
		check_belts()
		for k, _ in pairs(game.players) do
			belt_brush.players[k] = {group_id = 0}
		end
	end
end

local function get_len(a, b)
	local length = a.x - b.x
	if length == 0 then length = a.y - b.y end
	return math.abs(length)
end

local function get_direction(pre_build, current_build)
	if current_build.x == pre_build.x then
		if current_build.y == pre_build.y - 1 then return defines.direction.north end --0
		if current_build.y == pre_build.y + 1 then return defines.direction.south end --4
	end
	if current_build.y == pre_build.y then
		if current_build.x == pre_build.x - 1 then return defines.direction.west end --6
		if current_build.x == pre_build.x + 1 then return defines.direction.east end --2
	end
	return - 1
end

---- (such parameters are bad actually...)
local function change_player_belt_and_button(player, reset, reverse)
	local button = player.gui.top.belt_brush_top_button
	if not (button and button.valid) then return end

	local belt_groups = global.belt_brush.belt_groups
	local player_data = global.belt_brush.players[player.index]
	if reset or not player_data.group_id then
		player_data.group_id = 1
	elseif reverse then
		player_data.group_id = player_data.group_id - 1
		if player_data.group_id == 0 then player_data.group_id = #belt_groups end
	else
		player_data.group_id = (player_data.group_id + 1) % (#belt_groups + 1)
		if player_data.group_id == 0 then player_data.group_id = 1 end
	end

	if debug_mode then game.print("CHANGE: " .. serpent.line(belt_groups[player_data.group_id])) end

	local sprite = belt_groups[player_data.group_id].belt.name
	if player.gui.is_valid_sprite_path("item/" .. sprite) then
		button.sprite = "item/" .. sprite
	else
		button.sprite = "utility/questionmark"
	end

	-- if belt_brush_top_button.sprite ~= "item/substation" then
	--     for k, group in pairs(belt_groups) do
	--         if belt_brush_top_button.sprite == "item/" .. group.belt.name then
	--             belt_brush_top_button.sprite = "item/" .. belt_groups[k % #belt_groups + 1].belt.name
	--             break
	--         end
	--     end
	-- else
	--     belt_brush_top_button.sprite = "item/" .. global.belts.belt[1]
	-- end
end

-- local function create_belt_brush_gui(player)
--  local gui = player.gui.left
--  local belt_brush = player.gui.left.belt_brush
--  if belt_brush then
--      belt_brush.destroy()
--      return
--  end
--  local holding_table = gui.add{type = "table", name = "belt_brush", column_count = 1}
--  holding_table.style.left_padding = 5
--  holding_table.style.top_padding = 5
--  holding_table.style.bottom_padding = 2
--  holding_table.style.right_padding = 2
--  local label = holding_table.add{type = "label", caption = {"mod-name.belt-brush"}}
--  local items = {{"technology-name.automated-construction"}}
--  for _, data in pairs(global.belt_brush.belt_groups) do
--      table.insert(items, game.item_prototypes[data.belt.name].localised_name)
--  end
--  local player_data = global.belt_brush.players[player.index]
--  local selected_index
--  if player_data.selected then
--      selected_index = player_data.selected + 1
--  else
--      selected_index = 1
--  end
--  local drop_down = holding_table.add{type = "drop-down", name = "belt_brush_drop_down", items = items, selected_index = selected_index}
-- end

local function create_top_button(player)
	local gui = player.gui.top
	if gui.belt_brush_top_button then
		gui.belt_brush_top_button.destroy()
	end

	local belt_groups = global.belt_brush.belt_groups
	global.belt_brush.players[player.index] = global.belt_brush.players[player.index] or {group_id = 1}
	local player_data = global.belt_brush.players[player.index]
	if player_data.group_id < 1 then player_data.group_id = 1 end
	local sprite = belt_groups[player_data.group_id].belt.name
	local button = gui.add({type = "sprite-button", name = "belt_brush_top_button", sprite = "item/" .. sprite})
	button.style.minimal_height = 38
	button.style.minimal_width = 38
	button.style.top_padding = 0
	button.style.left_padding = 0
	button.style.right_padding = 0
	button.style.bottom_padding = 0
end

local function destroy_top_button(player)
	local gui = player.gui.top
	if gui.belt_brush_top_button then
		gui.belt_brush_top_button.destroy()
	end
end

local function on_configuration_changed()
	on_init()
	for index, player in pairs(game.players) do
		-- create_top_button(player)
		change_player_belt_and_button(player, true)
	end
end

local function build(player, building, direction, is_ghost)
	-- if not build.fast then if debug_mode then game.print("!fast") end end
	local group_id = global.belt_brush.players[player.index].group_id
	local belt_groups = global.belt_brush.belt_groups
	if building.is_placeable and not building.ug then
		if direction == -1 then direction = 0 end
		if group_id == 0 then
			for i = #belt_groups, 1, -1 do
				local name = belt_groups[i].belt.name
				if debug_mode then game.print(name) end
				if is_ghost then
					player.surface.create_entity{name = "entity-ghost", inner_name = name, position = building, player = player, fast_replace = true, force = player.force, direction = direction}
				else
					if player.remove_item({name = name, count = 1}) ~= 0 then
						player.surface.create_entity{name = name, position = building, player = player, fast_replace = true, force = player.force, direction = direction}
						break
					elseif i == 1 then
						player.surface.create_entity{name = "entity-ghost", inner_name = name, position = building, player = player, fast_replace = true, force = player.force, direction = direction}
					end
				end
			end
		else
			local name = belt_groups[group_id].belt.name
			if is_ghost then
				player.surface.create_entity{name = "entity-ghost", inner_name = name, position = building, player = player, fast_replace = true, force = player.force, direction = direction}
			else
				if player.remove_item({name = name, count = 1}) ~= 0 then
					player.surface.create_entity{name = name, position = building, player = player, fast_replace = true, force = player.force, direction = direction}
				else
					player.surface.create_entity{name = "entity-ghost", inner_name = name, position = building, player = player, fast_replace = true, force = player.force, direction = direction}
				end
			end
		end
	end
end

local function build_splitter(event, pre_pre_build, current_build, direction, is_ghost)
	local player = game.players[event.player_index]
	local belt_groups = global.belt_brush.belt_groups
	if direction == -1 then direction = 0 end
	local relative = get_direction(pre_pre_build, current_build)
	local swp = 0
	if relative == 6 or relative == 0 then swp = 1 else swp = -1 end
	-- if direction == 0 then
	--     if relative == 6 then swp = 1 else swp = -1 end
	-- elseif direction == 2 then
	--     if relative == 0 then swp = 2 else swp = -2 end
	-- elseif direction == 4 then
	--     if relative == 6 then swp = 3 else swp = -3 end
	-- else
	--     if relative == 0 then swp =  4 else swp = -4 end
	-- end
	if debug_mode then game.print("dir: " .. direction .. ", rel: " .. relative .. ", swp: " .. swp) end
	if swp > 0 then current_build = pre_pre_build end

	if player.surface.can_fast_replace{name = "splitter", position = current_build, direction = direction, force = player.force} or true then -- check
		local group_id = global.belt_brush.players[event.player_index].group_id
		if group_id == 0 then
			for i = #belt_groups, 1, -1 do
				local name = belt_groups[i].splitter.name
				if debug_mode then game.print(name) end
				if is_ghost then
					player.surface.create_entity{name = "entity-ghost", inner_name = name, position = current_build, player = player, fast_replace = true, force = player.force, direction = direction}
				else
					if player.remove_item({name = name, count = 1}) ~= 0 then
						player.surface.create_entity{name = name, position = current_build, player = player, fast_replace = true, force = player.force, direction = direction}
						return true
					elseif i == 1 then
						player.surface.create_entity{name = "entity-ghost", inner_name = name, position = current_build, player = player, fast_replace = true, force = player.force, direction = direction}
					end
				end
			end
		else
			local name = belt_groups[group_id].splitter.name
			if is_ghost then
				player.surface.create_entity{name = "entity-ghost", inner_name = name, position = current_build, player = player, fast_replace = true, force = player.force, direction = direction}
			else
				if player.remove_item({name = name, count = 1}) ~= 0 then
					player.surface.create_entity{name = name, position = current_build, player = player, fast_replace = true, force = player.force, direction = direction}
					return true
				else
					player.surface.create_entity{name = "entity-ghost", inner_name = name, position = current_build, player = player, fast_replace = true, force = player.force, direction = direction}
				end
			end
		end
	else
		player.print("Failed to fast-replace with splitter")
	end
end

-- local function rebuild_underground(event, uout, uin, puout, puin)
--  -- уже в линии
--  local maxlen = 5 -- max_ug_dist+1
--  local len = math.abs(uin.x-uout.x)
--  if len == 0 then len = math.abs(uin.y-uout.y) end
--  local plen = math.abs(puin.x-puout.x)
--  if plen == 0 then plen = math.abs(puin.y-puout.y) end
--  local dlen = math.abs(uin.x-puout.x)
--  if dlen == 0 then dlen = math.abs(uin.y-puout.y) end

--  -- if dlen < 2 then we're fucked
--  if dlen < 2 then if debug_mode then game.print("We're fucked!") end return end
--  if len+plen < maxlen then --  we can merge ug's
--      _build(uout,"up")
--      _kill(uin .. pout)
--  elseif len <= maxlen then -- we can build it
--      _build(uin,"in")
--      _build(uout,"up")
--  end

-- end

local function build_underground_belt(player, pre_build, current_build, is_ghost)
	--WIP
	local player_data = global.belt_brush.players[player.index]
	local belt_groups = global.belt_brush.belt_groups
	local group_id = player_data.group_id -- check (https://mods.factorio.com/mod/belt-brush/discussion/5f4d2fb89d08f07fc55808ec)
	if debug_mode then
		game.print("id " .. group_id)
		game.print("gp " .. serpent.line(belt_groups[group_id]))
	end
	local ulen = belt_groups[group_id].underground_length or 5 -- check (https://mods.factorio.com/mod/belt-brush/discussion/5ef240d5d1b96e000c4e9311)
	if ulen < get_len(current_build, pre_build) then player.print("Too far") return end
	local name = belt_groups[group_id].underground_belt.name

	local player = game.players[player.index]
	if player_data.puin and player_data.puin.valid and player_data.puin.position == pre_build and player_data.puout then
		player.mine_entity(player_data.puout)
		-- player.insert({name = player_data.puout.name, count = 1})
		-- player_data.puout.destroy()
	end

	local surface = player.surface
	local direction = current_build.inp
	if direction == -1 then direction = 0 end
	if pre_build.is_placeable and current_build.is_placeable then
		if is_ghost then
			player_data.puin = surface.create_entity{name = "entity-ghost", inner_name = name, position = pre_build, fast_replace = true, player = player, force = player.force, direction = direction, type = "input"}
			player_data.puout = surface.create_entity{name = "entity-ghost", inner_name = name, position = current_build, fast_replace = true, player = player, force = player.force, direction = direction, type = "output"}
		else
			if player.remove_item({name = name, count = 1}) ~= 0 then
				player_data.puin = surface.create_entity{name = name, position = pre_build, fast_replace = true, player = player, force = player.force, direction = direction, type = "input"}
			else
				player_data.puin = surface.create_entity{name = "entity-ghost", inner_name = name, position = pre_build, fast_replace = true, player = player, force = player.force, direction = direction, type = "input"}
			end
			if player.remove_item({name = name, count = 1}) ~= 0 then
				player_data.puout = surface.create_entity{name = name, position = current_build, fast_replace = true, player = player, force = player.force, direction = direction, type = "output"}
			else
				player_data.puout = surface.create_entity{name = "entity-ghost", inner_name = name, position = current_build, fast_replace = true, player = player, force = player.force, direction = direction, type = "output"}
			end
		end
	end
end

local function tile_data(tile, pre_build, player)
	local position = tile.position
	local surface = player.surface
	if pre_build then
		position.inp = get_direction(pre_build, position)
	else
		position.inp = 1
	end
	local inp = position.inp
	if inp == -1 then inp = 0 end

	position.is_placeable = surface.can_place_entity{name = "transport-belt", position = position, direction = inp, force = player.force}
	position.fast = surface.can_fast_replace{name = "transport-belt", position = position, direction = inp, force = player.force}

	-- if debug_mode then game.print(serpent.line(position)) end

	return position
end

local function on_player_built_tile(event)
	if not (event.item and event.item.name == "belt-brush" and #event.tiles == 1) then return end
	local tile = event.tiles[1]
	local player_data = global.belt_brush.players[event.player_index]
	local pre_pre_pre_build = player_data.pre_pre_build
	local pre_pre_build = player_data.pre_build
	local pre_build = player_data.current_build --?
	local player = game.players[event.player_index]
	local current_build = tile_data(tile, pre_build, player)
	player_data.pre_pre_pre_build = pre_pre_pre_build
	player_data.pre_pre_build = pre_pre_build
	player_data.pre_build = pre_build
	player_data.current_build = current_build
	local surface = player.surface
	if pre_build then
		if surface.get_tile(pre_build.x, pre_build.y).name == "belt-brush-tile" then
			player.insert({name = "belt-brush", count = 1})
			surface.set_tiles({{name = surface.get_hidden_tile(pre_build), position = pre_build}})
		end
	end
	--[[ c.over = !empty(tile) ]]
	-- TODO: is_placeable positions for undergrounds:  player_data.uin, player_data.uout, (prev) player_data.puin, player_data.puout

	if pre_pre_pre_build then
		if debug_mode then game.print("pos:" .. serpent.line(current_build) .. " p:" .. serpent.line(pre_build) .. " pre_pre_build:" .. serpent.line(pre_pre_build) .. " pre_pre_pre_build:" .. serpent.line(pre_pre_pre_build)) end
		if current_build.inp == -1 and pre_build.inp ~= -1 then --jump, prev was not jump
			if pre_pre_build.fast or pre_pre_build.is_placeable then -- and p.fast
				build(player, pre_pre_build, pre_build.inp)
			end
		end
		if not current_build.is_placeable then -- ug
			if current_build.inp == -1 then
				--nop
			elseif pre_build.is_placeable then -- from ground to ug
				player_data.uin = pre_build
			elseif pre_build.inp ~= current_build.inp then -- ug to ug, turn
				player_data.uin = nil
			else -- ug to ug, line
				--nop
			end
		else
			if current_build.inp == -1 then
				--nop
			elseif pre_build.inp == -1 or current_build.inp ~= pre_build.inp then --second or rotated
				-- build(player, current_build, current_build.inp)
				build(player, pre_build, current_build.inp)
			else
				-- build(player, current_build, current_build.inp)
				build(player, pre_build, current_build.inp)
			end

			-- pre_build == player_data.uin
			if player_data.uin then
				if debug_mode then game.print("ug! uin:" .. serpent.line(player_data.uin)) end
				if pre_build.inp == current_build.inp then
					build_underground_belt(player, player_data.uin, current_build)
					-- player_data.puin = player_data.uin
					-- player_data.puout = current_build
					current_build.ug = true
				end
				player_data.uin = nil
			end
		end

		-- if current_build.inp ~= -1 and pre_build.inp == -1 and pre_pre_build.inp == current_build.inp then --split
		--     if get_direction(pre_pre_build, current_build) ~= -1 and get_direction(pre_pre_build, current_build) ~= current_build.inp and get_direction(current_build, pre_pre_build) ~= current_build.inp then
		if current_build.inp == -1 and pre_build.inp ~= -1 and get_direction(pre_pre_build, current_build) ~= -1 then
			if build_splitter(event, pre_pre_build, current_build, pre_build.inp) then
				current_build.is_placeable = false
			end
			if debug_mode then game.print("splitter!") end
		end
	end
end

local function on_built_entity(event)
	local tile = event.created_entity
	if not (tile and tile.name == "tile-ghost" and tile.ghost_name == "belt-brush-tile") then return end
	local player_data = global.belt_brush.players[event.player_index]
	local pre_pre_pre_build = player_data.pre_pre_build
	local pre_pre_build = player_data.pre_build
	local pre_build = player_data.current_build --?
	local player = game.players[event.player_index]
	local current_build = tile_data(tile, pre_build, player)
	player_data.pre_pre_pre_build = pre_pre_pre_build
	player_data.pre_pre_build = pre_pre_build
	player_data.pre_build = pre_build
	player_data.current_build = current_build
	local surface = player.surface
	--[[ c.over = !empty(tile) ]]
	-- TODO: is_placeable positions for undergrounds: player_data.uin, player_data.uout, (prev) player_data.puin, player_data.puout

	if pre_pre_pre_build then
		if debug_mode then game.print("pos:" .. serpent.line(current_build) .. " p:" .. serpent.line(pre_build) .. " pre_pre_build:" .. serpent.line(pre_pre_build) .. " pre_pre_pre_build:" .. serpent.line(pre_pre_pre_build)) end
		if current_build.inp == -1 and pre_build.inp ~= -1 then --jump, prev was not jump
			if pre_pre_build.fast or pre_pre_build.is_placeable then -- and p.fast
				build(player, pre_pre_build, pre_build.inp, true)
			end
		end
		if not current_build.is_placeable then -- ug
			if current_build.inp == -1 then
				--nop
			elseif pre_build.is_placeable then -- from ground to ug
				player_data.uin = pre_build
			elseif pre_build.inp ~= current_build.inp then -- ug to ug, turn
				player_data.uin = nil
			else -- ug to ug, line
				--nop
			end
		else
			if current_build.inp == -1 then
				--nop
			elseif pre_build.inp == -1 or current_build.inp ~= pre_build.inp then --second or rotated
				-- build(player, current_build, current_build.inp, true)
				build(player, pre_build, current_build.inp, true)
			else
				-- build(player, current_build, current_build.inp, true)
				build(player, pre_build, current_build.inp, true)
			end

			-- pre_build == player_data.uin
			if player_data.uin then
				if debug_mode then game.print("ug! uin:" .. serpent.line(player_data.uin)) end
				if pre_build.inp == current_build.inp then
					build_underground_belt(player, player_data.uin, current_build, true)
					-- player_data.puin = player_data.uin
					-- player_data.puout = current_build
					current_build.ug = true
				end
				player_data.uin = nil
			end
		end

		-- if current_build.inp ~= -1 and pre_build.inp == -1 and pre_pre_build.inp == current_build.inp then --split
		--     if get_direction(pre_pre_build, current_build) ~= -1 and get_direction(pre_pre_build, current_build) ~= current_build.inp and get_direction(current_build, pre_pre_build) ~= current_build.inp then
		if current_build.inp == -1 and pre_build.inp ~= -1 and get_direction(pre_pre_build, current_build) ~= -1 then
			if build_splitter(event, pre_pre_build, current_build, pre_build.inp, true) then
				current_build.is_placeable = false
			end
			if debug_mode then game.print("splitter!") end
		end
	end
end

local function update_player_data(event)
	local belt_brush = global.belt_brush
	if belt_brush.players[event.player_index] == nil then
		belt_brush.players[event.player_index] = {group_id = 1}
	elseif belt_brush.players[event.player_index].group_id == nil then
		belt_brush.players[event.player_index].group_id = 1
	end
end

local function delete_player_data(event)
	global.belt_brush.players[event.player_index] = nil
end

local function clear_player_data(event)
	global.belt_brush.players[event.player_index] = {group_id = 1}
end

local function on_player_left_game(event)
	clear_player_data(event)
end

-- local gui_functions =
-- {
--     ["belt_brush_top_button"] = change_player_belt_and_button,
--         --function(event) create_belt_brush_gui(game.players[event.player_index]) end,
--     -- ["belt_brush_drop_down"] = function(event)
--     --     local selected_index = game.players[event.player_index].gui.left.belt_brush.belt_brush_drop_down.selected_index - 1
--     --     if selected_index > 0 then
--     --         global.belt[event.player_index].selected = selected_index
--     --     else
--     --         global.belt[event.player_index].selected = nil
--     --     end
--     -- end,
-- }

local function on_gui_click(event)
	local gui = event.element
	local player = game.players[event.player_index]
	if not (player and player.valid and gui and gui.valid) then return end
	if gui.name ~= "belt_brush_top_button" then return end
	change_player_belt_and_button(player)
end

-- local function on_gui_selection_state_changed(event)
--     local gui = event.element
--     local player = game.players[event.player_index]
--
--     if not (player and player.valid and gui and gui.valid) then return end
--
--     if gui.name then
--         local button_function = gui_functions[gui.name]
--         if button_function then
--             button_function(event)
--             return
--         end
--     end
-- end

local function on_runtime_mod_setting_changed(event)
	if event.setting_type ~= "runtime-global" then return end

	if event.setting == "belt-brush_debug-mode" then
		debug_mode = settings.global[event.setting].value
	end
end

local function on_player_cursor_stack_changed(event)
	local player = game.players[event.player_index]
	local button = player.gui.top.belt_brush_top_button
	if (button == nil) then
		if debug_mode then game.print("no button") end
		create_top_button(player)
		button = player.gui.top.belt_brush_top_button
	end
	
	button.visible = true
	local stack = player.cursor_stack
	local item = stack and stack.valid_for_read and stack.name

	if (player.mod_settings["belt-brush_hide-icon"].value) then
		if item == "belt-brush" then
			if debug_mode then game.print("show button!") end
		else
			button.visible = false
			if debug_mode then 
				game.print("hide button ".. serpent.line({
					stack = (stack ~= nil),
					valid = (stack ~= nil) and (stack.valid_for_read),
					item = item,
					setting = player.mod_settings["belt-brush_hide-icon"]
				}))
			end
		end
	else
		if debug_mode then game.print("alvays show button! "..serpent.line(player.mod_settings["belt-brush_hide-icon"])) end
	end
end

local function player_picked_prev_belt(event)
	local player = game.players[event.player_index]
	if not (player and player.valid) then return end
	local stack = player.cursor_stack
	local item = stack and stack.valid_for_read and stack.name
	if item ~= "belt-brush" then return end

	local belt_groups = global.belt_brush.belt_groups
	local player_data = global.belt_brush.players[event.player_index]
	change_player_belt_and_button(player, false, true)
end

local function player_picked_next_belt(event)
	local player = game.players[event.player_index]
	if not (player and player.valid) then return end
	local stack = player.cursor_stack
	local item = stack and stack.valid_for_read and stack.name
	if item ~= "belt-brush" then return end

	local belt_groups = global.belt_brush.belt_groups
	local player_data = global.belt_brush.players[event.player_index]
	change_player_belt_and_button(player)
end

script.on_init(on_init)
script.on_configuration_changed(on_configuration_changed)
script.on_event(defines.events.on_gui_click, on_gui_click)
script.on_event(defines.events.on_built_entity, on_built_entity)
script.on_event(defines.events.on_player_joined_game, update_player_data)
script.on_event(defines.events.on_player_created, update_player_data)
script.on_event(defines.events.on_player_built_tile, on_player_built_tile)
script.on_event(defines.events.on_player_changed_force, clear_player_data)
script.on_event(defines.events.on_player_changed_surface, clear_player_data)
script.on_event(defines.events.on_player_left_game, on_player_left_game)
script.on_event(defines.events.on_player_removed, delete_player_data)
script.on_event(defines.events.on_runtime_mod_setting_changed, on_runtime_mod_setting_changed)
-- script.on_event(defines.events.on_gui_selection_state_changed, on_gui_selection_state_changed)
script.on_event(defines.events.on_player_cursor_stack_changed, on_player_cursor_stack_changed)
script.on_event("belt-brush_scroll-previous", player_picked_prev_belt)
script.on_event("belt-brush_scroll-next", player_picked_next_belt)

-- tile
local belt_brush_tile = util.table.deepcopy(data.raw["tile"]["stone-path"])
belt_brush_tile.name = "belt-brush-tile"
belt_brush_tile.decorative_removal_probability = 0
belt_brush_tile.walking_speed_modifier = nil

-- item
local belt_brush = util.table.deepcopy(data.raw["item"]["stone-brick"])
belt_brush.name = "belt-brush"
belt_brush.icon = "__belt-brush__/graphics/belt-brush.png"
belt_brush.icon_mipmaps = nil
belt_brush.result = belt_brush_tile.name
belt_brush.order = "a[" .. belt_brush.name  .. "]"
belt_brush.place_as_tile.result = belt_brush_tile.name
belt_brush.stack_size = 500
belt_brush_tile.minable = {hardness = 0.1, mining_time = 0.01, result = belt_brush.name}

-- recipe
local belt_brush_recipe = util.table.deepcopy(data.raw["recipe"]["stone-brick"])
belt_brush_recipe.name = "belt-brush"
belt_brush_recipe.result = belt_brush.name
belt_brush_recipe.ingredients = {}
belt_brush_recipe.category = nil
belt_brush_recipe.energy_required = nil
belt_brush_recipe.result_count = 10

data:extend({belt_brush_tile, belt_brush, belt_brush_recipe})

data:extend{
	{
		type = "custom-input",
		name = "belt-brush_scroll-previous",
		key_sequence = "SHIFT + mouse-wheel-down",
		order = "a"
	},
	{
		type = "custom-input",
		name = "belt-brush_scroll-next",
		key_sequence = "SHIFT + mouse-wheel-up",
		order = "b"
	}
}